package org.j2os.controller;

import org.j2os.model.entity.*;
import org.j2os.model.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
@RequestMapping("/checkSche")
public class CheckScheController {
    @Autowired
    private EmployeeSkillServiceIX EmpSkillservice;
    @Autowired
    private PlaneServiceIX planeService;
    @Autowired
    private CheckupServiceIX checkupService;
    @Autowired
    private CheckupScheduleServiceIX checkupScheduleService;

    @RequestMapping("/save.do")
    public String save(@RequestParam("checkupId") int checkupId,@RequestParam("planeId") int planeId,
                       @RequestParam("empSkillId") int empSkillId,@RequestParam("done") int isDone) {
        Checkup checkup = new Checkup();
        checkup.setId(checkupId);

        Plane plane = new Plane();
        plane.setId(planeId);

        EmpSkill empSkill = new EmpSkill();
        empSkill.setId(empSkillId);

        CheckupSchedule checkupSchedule = new CheckupSchedule();
        checkupSchedule.setCheckup(checkup);
        checkupSchedule.setPlane(plane);
        checkupSchedule.setEmpSkill(empSkill);
        System.out.println(isDone);
        checkupSchedule.setDone((isDone == 1) ? true : false);
        checkupScheduleService.save(checkupSchedule);

        return "redirect:/checkSche/findAll.do";
    }
    @RequestMapping("/update.do")
    public String update(@ModelAttribute CheckupSchedule checkupSchedule)
    {
        checkupScheduleService.update(checkupSchedule);
        return "redirect:/checkSche/findAll.do";
    }
    @RequestMapping(value = "/delete.do")
    public String delete(@ModelAttribute CheckupSchedule checkupSchedule)
    {
        checkupScheduleService.delete(checkupSchedule);
        return "redirect:/checkSche/findAll.do";
    }
    @RequestMapping("/findAll.do")
    public String findAll(HttpServletRequest request)
    {
        request.setAttribute("list",checkupScheduleService.findAll());
        request.setAttribute("planeList",planeService.findAll());
        request.setAttribute("checkupList",checkupService.findAll());
        request.setAttribute("empSkillList",EmpSkillservice.findAll());
        return "checkSche/checkSche";
    }
}
