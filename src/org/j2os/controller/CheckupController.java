package org.j2os.controller;

import org.j2os.model.entity.Checkup;
import org.j2os.model.entity.Skill;
import org.j2os.model.service.CheckupServiceIX;
import org.j2os.model.service.SkillServiceIX;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;


@Controller
@RequestMapping("/checkup")
public class CheckupController {
    @Autowired
    private SkillServiceIX skillService;

    @Autowired
    private CheckupServiceIX checkupService;

    @RequestMapping("/save.do")
    public String save(@RequestParam("skillId") int skillId, @RequestParam("description") String description)
    {
        Skill skill = new Skill();
        skill.setId(skillId);

        Checkup checkup = new Checkup();
        checkup.setSkill(skill);
        checkup.setDescription(description);
        checkupService.save(checkup);

        return "redirect:/checkup/findAll.do";
    }
    @RequestMapping("/update.do")
    public String update(@ModelAttribute Checkup checkup)
    {
        checkupService.update(checkup);
        return "redirect:/checkup/findAll.do";
    }
    @RequestMapping(value = "/delete.do")
    public String delete(@ModelAttribute Checkup checkup)
    {
        checkupService.delete(checkup);
        return "redirect:/checkup/findAll.do";
    }
    @RequestMapping("/findAll.do")
    public String findAll(HttpServletRequest request) {
        request.setAttribute("list", checkupService.findAll());
        request.setAttribute("skillList", skillService.findAll());
        return "checkup/checkup";
    }
}
