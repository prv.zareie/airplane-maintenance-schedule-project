package org.j2os.controller;

import org.j2os.model.entity.Employee;
import org.j2os.model.service.EmployeeService;
import org.j2os.model.service.EmployeeServiceIX;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/employee")
public class EmployeeController {
    @Autowired
    private EmployeeServiceIX service;
    @RequestMapping("/save.do")
    public String save(@ModelAttribute Employee employee)
    {
        service.save(employee);
        return "redirect:/employee/findAll.do";
    }
    @RequestMapping("/update.do")
    public String update(@ModelAttribute Employee employee)
    {
        service.update(employee);
        return "redirect:/employee/findAll.do";
    }
    @RequestMapping(value = "/delete.do")
    public String delete(@ModelAttribute Employee employee)
    {
        service.delete(employee);
        return "redirect:/employee/findAll.do";
    }
    @RequestMapping("/findAll.do")
    public String findAll(HttpServletRequest request)
    {
        request.setAttribute("list",service.findAll());
        return "employee/employee";
    }
}
