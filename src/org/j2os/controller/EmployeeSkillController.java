package org.j2os.controller;

import org.j2os.model.entity.EmpSkill;
import org.j2os.model.entity.Employee;
import org.j2os.model.entity.Skill;
import org.j2os.model.service.*;
import org.j2os.model.service.EmployeeSkillServiceIX;
import org.j2os.model.service.SkillService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/empSkill")
public class EmployeeSkillController {
    @Autowired
    private EmployeeSkillServiceIX service;
    @Autowired
    private EmployeeServiceIX employeeService;
    @Autowired
    private SkillServiceIX skillService;

    @RequestMapping("/save.do")
    public String save(@RequestParam("employeeId") int employeeId,@RequestParam("skillId") int skillId)
    {
        Employee employee = new Employee();
        employee.setId(employeeId);

        Skill skill = new Skill();
        skill.setId(skillId);

        EmpSkill empSkill = new EmpSkill();
        empSkill.setSkill(skill);
        empSkill.setEmployee(employee);
        service.save(empSkill);

        return "redirect:/empSkill/findAll.do";
    }
    @RequestMapping("/update.do")
    public String update(@ModelAttribute EmpSkill empSkill)
    {
        service.update(empSkill);
        return "redirect:/empSkill/findAll.do";
    }
    @RequestMapping(value = "/delete.do")
    public String delete(@ModelAttribute EmpSkill empSkill)
    {
        service.delete(empSkill);
        return "redirect:/empSkill/findAll.do";
    }
    @RequestMapping("/findAll.do")
    public String findAll(HttpServletRequest request)
    {
        request.setAttribute("list",service.findAll());
        request.setAttribute("empList",employeeService.findAll());
        request.setAttribute("skillList",skillService.findAll());
        return "empSkill/empSkill";
    }
}
