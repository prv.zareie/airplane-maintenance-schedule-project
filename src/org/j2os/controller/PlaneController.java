package org.j2os.controller;

import org.j2os.model.entity.Plane;
import org.j2os.model.service.PlaneService;
import org.j2os.model.service.PlaneServiceIX;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/plane")
public class PlaneController {
    @Autowired
    private PlaneServiceIX service;
    @RequestMapping("/save.do")
    public String save(@ModelAttribute Plane plane)
    {
        service.save(plane);
        return "redirect:/plane/findAll.do";
    }
    @RequestMapping("/update.do")
    public String update(@ModelAttribute Plane plane)
    {
        service.update(plane);
        return "redirect:/plane/findAll.do";
    }
    @RequestMapping(value = "/delete.do")
    public String delete(@ModelAttribute Plane plane)
    {
        service.delete(plane);
        return "redirect:/plane/findAll.do";
    }
    @RequestMapping("/findAll.do")
    public String findAll(HttpServletRequest request) {
        request.setAttribute("list", service.findAll());
        return "plane/plane";
    }
}
