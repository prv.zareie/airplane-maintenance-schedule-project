package org.j2os.controller;

import org.j2os.model.entity.Skill;
import org.j2os.model.service.SkillService;
import org.j2os.model.service.SkillServiceIX;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/skill")
public class SkillController {
    @Autowired
    private SkillServiceIX service;
    @RequestMapping("/save.do")
    public String save(@ModelAttribute Skill skill)
    {
        service.save(skill);
        return "redirect:/skill/findAll.do";
    }
    @RequestMapping("/update.do")
    public String update(@ModelAttribute Skill skill)
    {
        service.update(skill);
        return "redirect:/skill/findAll.do";
    }
    @RequestMapping(value = "/delete.do")
    public String delete(@ModelAttribute Skill skill)
    {
        service.delete(skill);
        return "redirect:/skill/findAll.do";
    }
    @RequestMapping("/findAll.do")
    public String findAll(HttpServletRequest request) {
        request.setAttribute("list", service.findAll());
        return "skill/skill";
    }
}
