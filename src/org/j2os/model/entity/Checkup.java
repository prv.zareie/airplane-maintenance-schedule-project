package org.j2os.model.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity(name = "Checkup")
@Table(name = "Checkup")
public class Checkup implements Serializable {
    @Id
    @SequenceGenerator(name = "checkupSeq", sequenceName = "CHECKUP_SEQ", initialValue = 1, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "checkupSeq")
    @Column(name = "ID", columnDefinition = "NUMBER")
    private int id;

    @Column(name="Description", columnDefinition = "VARCHAR2(100)")
    private String description;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "skillId")
    private Skill skill;

    public Checkup(String description, Skill skill) {
        this.description = description;
        this.skill = skill;
    }

    public Checkup() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Skill getSkill() {
        return skill;
    }

    public void setSkill(Skill skill) {
        this.skill = skill;
    }
}
