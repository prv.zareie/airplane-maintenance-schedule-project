package org.j2os.model.entity;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity(name = "CheckupSchedule")
@Table(name = "CheckupSchedule")
public class CheckupSchedule {

    @Id
    @Column(name = "ID", columnDefinition = "NUMBER")
    @SequenceGenerator(name = "CheckupScheduleseq", sequenceName = "CHECKUPSCHEDULE_SEQ", initialValue = 1, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CheckupScheduleseq")
    private int id;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "planeId")
    private Plane plane;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "empSkillId")
    private EmpSkill empSkill;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "checkupId")
    private Checkup checkup;

    @Column(name="IsDone", columnDefinition = "Number(1)")
    private boolean done;

    public CheckupSchedule(Plane plane, EmpSkill empSkill, Checkup checkup,boolean isDone) {
        this.plane = plane;
        this.empSkill = empSkill;
        this.checkup = checkup;
        this.done = isDone;
    }

    public CheckupSchedule() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Plane getPlane() {
        return plane;
    }

    public void setPlane(Plane plane) {
        this.plane = plane;
    }

    public EmpSkill getEmpSkill() {
        return empSkill;
    }

    public void setEmpSkill(EmpSkill empSkill) {
        this.empSkill = empSkill;
    }

    public Checkup getCheckup() {
        return checkup;
    }

    public void setCheckup(Checkup checkup) {
        this.checkup = checkup;
    }

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }
}
