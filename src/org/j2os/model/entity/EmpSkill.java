package org.j2os.model.entity;

import javax.persistence.*;

@Entity(name = "EmpSkill")
@Table(name = "EmpSkill")
public class EmpSkill {

    @Id
    @Column(name = "ID", columnDefinition = "NUMBER")
    @SequenceGenerator(name = "emplSkillseq", sequenceName = "EMP_SKILL_SEQ", initialValue = 1, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "emplSkillseq")
    private int id;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "employeeId")
    private Employee employee;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "skillId")
    private Skill skill;

    public EmpSkill(Employee employee, Skill skill) {
        this.employee = employee;
        this.skill = skill;
    }

    public EmpSkill() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Skill getSkill() {
        return skill;
    }

    public void setSkill(Skill skill) {
        this.skill = skill;
    }
}
