package org.j2os.model.entity;

import javax.persistence.*;

@Entity(name = "EMPLOYEE")
@Table(name = "EMPLOYEE")
public class Employee {

    @Id
    @Column(name = "ID", columnDefinition = "NUMBER")
    @SequenceGenerator(name = "employeeseq", sequenceName = "EMPLOYEE_SEQ", initialValue = 1, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "employeeseq")
    private int id;

    @Column(name="NAME", columnDefinition = "VARCHAR2(100)")
    private String name;

    @Column(name="FAMILY", columnDefinition = "VARCHAR2(100)")
    private String family;

    @Column(name="NATIONALCODE", columnDefinition = "VARCHAR2(10)")
    private String nationalcode;

    public Employee(int id, String name, String family, String nationalcode) {
        this.id = id;
        this.name = name;
        this.family = family;
        this.nationalcode = nationalcode;
    }

    public Employee() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFamily() {
        return family;
    }

    public void setFamily(String family) {
        this.family = family;
    }

    public String getNationalcode() {
        return nationalcode;
    }

    public void setNationalcode(String nationalcode) {
        this.nationalcode = nationalcode;
    }
}
