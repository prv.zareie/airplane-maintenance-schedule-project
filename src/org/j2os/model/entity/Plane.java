package org.j2os.model.entity;

import javax.persistence.*;

@Entity(name = "Plane")
@Table(name = "Plane")
public class Plane {

    @Id
    @SequenceGenerator(name = "planeSeq", sequenceName = "PLANE_SEQ", initialValue = 1, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "planeSeq")
    @Column(name = "ID", columnDefinition = "NUMBER")
    private int id;

    @Column(name="Code", columnDefinition = "VARCHAR2(100)")
    private String code;

    public Plane(String code) {
        this.code = code;
    }

    public Plane() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
