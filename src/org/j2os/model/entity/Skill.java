package org.j2os.model.entity;

import javax.persistence.*;

@Entity(name = "Skill")
@Table(name = "Skill")
public class Skill {

    @Id
    @SequenceGenerator(name = "skillSeq", sequenceName = "SKILL_SEQ", initialValue = 1, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "skillSeq")
    @Column(name = "ID", columnDefinition = "NUMBER")
    private int id;

    @Column(name="Description", columnDefinition = "VARCHAR2(100)")
    private String description;

    public Skill(String description) {
        this.description = description;
    }

    public Skill() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
