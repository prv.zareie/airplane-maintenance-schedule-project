package org.j2os.model.repository;

import org.springframework.stereotype.Repository;

import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Repository
public class JpaRepository<ENTITY> {
    @PersistenceContext
    private EntityManager entityManager;

    public void save(ENTITY entity) {
        entityManager.persist(entity);
    }

    public void update(ENTITY entity) {
        entityManager.merge(entity);
    }

    public void delete(ENTITY entity) {
        entity = entityManager.merge(entity);
        entityManager.remove(entity);
    }

    public ENTITY findOne(Class<ENTITY> aClass, int id) {
        return entityManager.find(aClass, id);
    }

    public List<ENTITY> findAll(Class<ENTITY> aClass) {
        Query query = entityManager.createQuery("select entity from " + aClass.getAnnotation(Entity.class).name() + " entity");
        return query.getResultList();
    }
}
