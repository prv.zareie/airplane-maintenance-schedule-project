package org.j2os.model.service;

import org.j2os.model.entity.*;
import org.j2os.model.repository.JpaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class CheckupScheduleService implements CheckupScheduleServiceIX {
    @Autowired
    private JpaRepository repository;

    public void save(CheckupSchedule checkupSchedule) {
        Plane plane = (Plane)repository.findOne(Plane.class,checkupSchedule.getPlane().getId());
        repository.update(plane);
        checkupSchedule.setPlane(plane);

        Checkup checkup = (Checkup) repository.findOne(Checkup.class,checkupSchedule.getCheckup().getId());
        repository.update(checkup);
        checkupSchedule.setCheckup(checkup);

        EmpSkill empSkill = (EmpSkill) repository.findOne(EmpSkill.class,checkupSchedule.getCheckup().getId());
        repository.update(empSkill);
        checkupSchedule.setEmpSkill(empSkill);

        repository.save(checkupSchedule);
    }

    public void update(CheckupSchedule checkupSchedule) {
        repository.update(checkupSchedule);
    }

    public void delete(CheckupSchedule checkupSchedule) {
        repository.delete(checkupSchedule);
    }

    public CheckupSchedule findOne(CheckupSchedule checkupSchedule) {
        return (CheckupSchedule) repository.findOne(CheckupSchedule.class, checkupSchedule.getId());
    }

    public List<CheckupSchedule> findAll() {
        return repository.findAll(CheckupSchedule.class);
    }
}
