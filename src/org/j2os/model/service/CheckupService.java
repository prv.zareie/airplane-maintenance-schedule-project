package org.j2os.model.service;

import org.j2os.model.entity.Checkup;
import org.j2os.model.entity.Skill;
import org.j2os.model.repository.JpaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.List;

@Service
@Transactional
public class CheckupService implements CheckupServiceIX {

    @Autowired
    private JpaRepository repository;


    public void save(Checkup object) {

        Skill skill = (Skill)repository.findOne(Skill.class,object.getSkill().getId());
        repository.update(skill);
        object.setSkill(skill);

        repository.save(object);
    }


    public void update(Checkup object) {
        repository.update(object);
    }


    public void delete(Checkup object) {
        repository.delete(object);
    }


    public Checkup findOne(Checkup object) {
        return (Checkup)repository.findOne(Checkup.class, object.getId());
    }


    public List<Checkup> findAll() {
        return repository.findAll(Checkup.class);
    }

    public void save1(Checkup checkup){}
}
