package org.j2os.model.service;

import org.j2os.model.entity.Checkup;

import java.util.List;

public interface CheckupServiceIX {
    void save(Checkup object);
    void update(Checkup object);
    void delete(Checkup object);
    Checkup findOne(Checkup object);
    List<Checkup> findAll();
}
