package org.j2os.model.service;

import org.j2os.model.entity.Checkup;
import org.j2os.model.entity.Employee;
import org.j2os.model.repository.JpaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class EmployeeService implements EmployeeServiceIX  {
    @Autowired
    private JpaRepository<Employee> repository;

    public void save(Employee employee) {
        repository.save(employee);
    }

    public void update(Employee employee) {
        repository.update(employee);
    }

    public void delete(Employee employee) {
        repository.delete(employee);
    }

    public Employee findOne(Employee employee) {
        return repository.findOne(Employee.class, employee.getId());
    }

    public List<Employee> findAll() {
        return repository.findAll(Employee.class);
    }
}
