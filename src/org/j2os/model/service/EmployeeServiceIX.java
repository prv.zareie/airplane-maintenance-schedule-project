package org.j2os.model.service;

import org.j2os.model.entity.Employee;

import java.util.List;

public interface EmployeeServiceIX {
    void save(Employee object);
    void update(Employee object);
    void delete(Employee object);
    Employee findOne(Employee object);
    List<Employee> findAll();
}
