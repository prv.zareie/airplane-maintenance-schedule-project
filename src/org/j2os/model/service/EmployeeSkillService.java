package org.j2os.model.service;

import org.j2os.model.entity.EmpSkill;
import org.j2os.model.entity.Employee;
import org.j2os.model.entity.Skill;
import org.j2os.model.repository.JpaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class EmployeeSkillService implements EmployeeSkillServiceIX  {
    @Autowired
    private JpaRepository repository;

    public void save(EmpSkill empSkill) {
        Employee employee = (Employee)repository.findOne(Employee.class,empSkill.getEmployee().getId());
        repository.update(employee);
        empSkill.setEmployee(employee);

        Skill skill = (Skill)repository.findOne(Skill.class,empSkill.getSkill().getId());
        repository.update(skill);
        empSkill.setSkill(skill);

        repository.save(empSkill);
    }

    public void update(EmpSkill empSkill) {
        repository.update(empSkill);
    }

    public void delete(EmpSkill empSkill) {
        repository.delete(empSkill);
    }

    public EmpSkill findOne(EmpSkill empSkill) {
        return (EmpSkill) repository.findOne(EmpSkill.class, empSkill.getId());
    }

    public List<EmpSkill> findAll() {
        return repository.findAll(EmpSkill.class);
    }
}
