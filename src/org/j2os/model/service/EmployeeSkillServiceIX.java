package org.j2os.model.service;

import org.j2os.model.entity.EmpSkill;

import java.util.List;

public interface EmployeeSkillServiceIX {
    void save(EmpSkill object);
    void update(EmpSkill object);
    void delete(EmpSkill object);
    EmpSkill findOne(EmpSkill object);
    List<EmpSkill> findAll();
}
