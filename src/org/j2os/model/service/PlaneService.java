package org.j2os.model.service;


import org.j2os.model.entity.Plane;
import org.j2os.model.repository.JpaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class PlaneService implements PlaneServiceIX {
    @Autowired
    private JpaRepository<Plane> repository;

    public void save(Plane object) {
        repository.save(object);
    }

    public void update(Plane object) {
        repository.update(object);
    }

    public void delete(Plane object) {
        repository.delete(object);
    }

    public Plane findOne(Plane object) {
        return repository.findOne(Plane.class, object.getId());
    }

    public List<Plane> findAll() {
        return repository.findAll(Plane.class);
    }
}
