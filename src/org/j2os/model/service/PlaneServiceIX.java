package org.j2os.model.service;

import org.j2os.model.entity.Plane;

import java.util.List;

public interface PlaneServiceIX {
    void save(Plane object);
    void update(Plane object);
    void delete(Plane object);
    Plane findOne(Plane object);
    List<Plane> findAll();
}
