package org.j2os.model.service;


import org.j2os.model.entity.Skill;
import org.j2os.model.repository.JpaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class SkillService implements SkillServiceIX {
    @Autowired
    private JpaRepository<Skill> repository;

    public void save(Skill skill) {
        repository.save(skill);
    }

    public void update(Skill skill) {
        repository.update(skill);
    }

    public void delete(Skill skill) {
        repository.delete(skill);
    }

    public Skill findOne(Skill skill) {
        return repository.findOne(Skill.class, skill.getId());
    }

    public List<Skill> findAll() {
        return repository.findAll(Skill.class);
    }
}
