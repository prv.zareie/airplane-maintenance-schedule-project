package org.j2os.model.service;

import org.j2os.model.entity.Skill;

import java.util.List;

public interface SkillServiceIX {
    void save(Skill object);
    void update(Skill object);
    void delete(Skill object);
    Skill findOne(Skill object);
    List<Skill> findAll();
}
