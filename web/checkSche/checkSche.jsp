<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Checkup Schedule Panel</title>
    <link rel="stylesheet" href="/assets/bootstrap.min.css">
    <script src="/assets/jquery.min.js"></script>
    <script src="/assets/bootstrap.min.js"></script>
</head>
<body>
<jsp:include page="/menu.jsp"/>
<div class="container">
    <div class="panel panel-primary">
        <div class="panel-heading">Checkup Schedule Panel</div>
        <div class="panel-body">
            <form action="/checkSche/save.do">
                <input type="text" placeholder="description" class="form-control" name="hh" id="hh"/>
             Plane:  <select name="planeId" style="width:150px" id="planeId" >
                   <c:forEach items="${planeList}" var="plane">
                       <option value="${plane.id}">
                           ${plane.code}
                       </option>
                   </c:forEach>
               </select>
                Checkup: <select name="checkupId" style="width:150px" id="checkupId" >
                <c:forEach items="${checkupList}" var="checkup">
                    <option value="${checkup.id}">
                            ${checkup.description} &nbsp; ${checkup.skill.description}
                    </option>
                </c:forEach>
            </select>
                EmpSkill: <select name="empSkillId" style="width:150px" id="empSkillId" >
                <c:forEach items="${empSkillList}" var="empSkill">
                    <option value="${empSkill.id}">
                            ${empSkill.skill.description} &nbsp; ${empSkill.employee.name}
                    </option>
                </c:forEach>
            </select>
                Status: <select name="done" style="width:150px" id="status" >
                <option value="1" >Done</option>
                <option value="0" >Not Done</option>
            </select>
                <input type="submit" class="btn btn-info" value="SAVE"/>
            </form>

            <table style="width: 100%" class="table table-hover">
                <thead>
                <tr>
                    <td>ID</td>
                    <td>Employee Full Name</td>
                    <td>Skill Description</td>
                    <td>Checkup Schedule Description</td>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${requestScope.list}" var="checkupSchedule">
                    <tr>
                        <form action="/checkSche/update.do">
                            <td><input readonly class="form-control" type="text" name="id" value="${checkupSchedule.id}"/></td>
                            <td><input readonly type="text" class="form-control" name="employee" value="${checkupSchedule.empSkill.employee.name} &nbsp; ${heckupSchedule.empSkill.employee.family} "/></td>
                            <td><input readonly type="text" class="form-control" name="skill" value="${checkupSchedule.empSkill.skill.description} "/></td>
                            <td><input type="checkbox" name="statusChecker" value="done" ${checkupSchedule.done == true ? 'checked' : ''} ></td>
                            <td><input type="submit" class="btn btn-info" value="UPDATE"/></td>
                            <td><input type="button" class="btn btn-danger" value="DELETE"
                                       onclick="removeCheckupSchedule(${checkupSchedule.id})"/></td>
                        </form>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
            <label id="label1" ></label>
        </div>
    </div>
</div>
<script>
    function removeCheckupSchedule(id) {
        if (confirm('Are You Sure?'))
            window.location = "/checkSche/delete.do?id=" + id;
    }
</script>
</body>
</html>
