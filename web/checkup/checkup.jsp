<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="th" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Checkup Panel</title>
    <link rel="stylesheet" href="/assets/bootstrap.min.css">
    <script src="/assets/jquery.min.js"></script>
    <script src="/assets/bootstrap.min.js"></script>
</head>
<body>
<jsp:include page="/menu.jsp"/>
<div class="container">
    <div class="panel panel-primary">
        <div class="panel-heading">Checkup Panel</div>
        <div class="panel-body">
            <form action="/checkup/save.do">
                <input type="text" placeholder="description" class="form-control" name = "description"/>
                skill: <select name="skillId" style="width:150px;height: 50px" id="skill" >
                <c:forEach items="${skillList}" var="skill">
                    <option value="${skill.id}">
                            ${skill.description}
                    </option>
                </c:forEach>
            </select>
                <input type="submit" class="btn btn-info" value="SAVE"/>
            </form>

            <table style="width: 100%" class="table table-hover">
                <thead>
                <tr>
                    <td>ID</td>
                    <td>Skill Description</td>
                    <td>Checkup Description</td>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${requestScope.list}" var="checkup">
                    <tr>
                        <form action="/checkup/update.do">
                            <td><input readonly class="form-control" type="text" name="id" value="${checkup.id}"/></td>
                            <td><input type="text" class="form-control" disabled="true" name="skill" value="${checkup.skill.description}"/></td>
                            <td><input type="text" class="form-control" name="description" value="${checkup.description}"/></td>
                            <td><input type="submit" class="btn btn-info" value="UPDATE"/></td>
                            <td><input type="button" class="btn btn-danger" value="DELETE"
                                       onclick="removeCheckup(${checkup.id})"/></td>
                        </form>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>
<script>
    function removeCheckup(id) {
        if (confirm('Are You Sure?'))
            window.location = "/checkup/delete.do?id=" + id;
    }
</script>
</body>
</html>
