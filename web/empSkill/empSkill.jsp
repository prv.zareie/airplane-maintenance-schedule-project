<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="th" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Employee Skill Panel</title>
    <link rel="stylesheet" href="/assets/bootstrap.min.css">
    <script src="/assets/jquery.min.js"></script>
    <script src="/assets/bootstrap.min.js"></script>
</head>
<body>
<jsp:include page="/menu.jsp"/>
<div class="container">
    <div class="panel panel-primary">
        <div class="panel-heading">Employee Skill Panel</div>
        <div class="panel-body">
            <form action="/empSkill/save.do">
                <input type="text" placeholder="description" class="form-control" name="hh" id="hh"/>
             employee:  <select name="employeeId" style="width:150px" id="employee" >
                   <c:forEach items="${empList}" var="employee">
                       <option value="${employee.id}">
                           ${employee.name} &nbsp;
                                   ${employee.family}
                       </option>
                   </c:forEach>
               </select>
                skill: <select name="skillId" style="width:150px" id="skill" >
                <c:forEach items="${skillList}" var="skill">
                    <option value="${skill.id}">
                            ${skill.description}
                    </option>
                </c:forEach>
            </select>
                <input type="submit" class="btn btn-info" value="SAVE"/>
            </form>

            <table style="width: 100%" class="table table-hover">
                <thead>
                <tr>
                    <td>ID</td>
                    <td>Employee Full Name</td>
                    <td>Skill Descritpion</td>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${requestScope.list}" var="empSkill">
                    <tr>
                        <form action="/empSkill/update.do">
                            <td><input readonly class="form-control" type="text" name="id" value="${empSkill.id}"/></td>
                            <td><input type="text" class="form-control" name="name" value="${empSkill.employee.name} &nbsp; ${empSkill.employee.family} "/></td>
                            <td><input type="text" class="form-control" name="skill" value="${empSkill.skill.description} "/></td>

                            <td><input type="button" class="btn btn-danger" value="DELETE"
                                       onclick="removeEmployeeSkill(${empSkill.id})"/></td>
                        </form>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>
<script>
    function removeEmployeeSkill(id) {
        if (confirm('Are You Sure?'))
            window.location = "/empSkill/delete.do?id=" + id;
    }
</script>
</body>
</html>
