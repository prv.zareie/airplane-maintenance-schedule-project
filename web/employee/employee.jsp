<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Employee Panel</title>
    <link rel="stylesheet" href="/assets/bootstrap.min.css">
    <script src="/assets/jquery.min.js"></script>
    <script src="/assets/bootstrap.min.js"></script>
</head>
<body>
<jsp:include page="/menu.jsp"/>
<div class="container">
    <div class="panel panel-primary">
        <div class="panel-heading">Employee Panel</div>
        <div class="panel-body">
            <form action="/employee/save.do">
                <input type="text" placeholder="name" class="form-control" name="name"/>
                <br/>
                <input type="text" placeholder="family" class="form-control" name="family"/>
                <br/>
                <input type="text" placeholder="nationalcode" class="form-control" name="nationalcode"/>
                <br/>
                <input type="submit" class="btn btn-info" value="SAVE"/>
            </form>

            <table style="width: 100%" class="table table-hover">
                <thead>
                <tr>
                    <td>ID</td>
                    <td>NAME</td>
                    <td>FAMILY</td>
                    <td>NATIONALCODE</td>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${requestScope.list}" var="employee">
                    <tr>
                        <form action="/employee/update.do">
                            <td><input readonly class="form-control" type="text" name="id" value="${employee.id}"/></td>
                            <td><input type="text" class="form-control" name="name" value="${employee.name}"/></td>
                            <td><input type="text" class="form-control" name="family" value="${employee.family}"/></td>
                            <td><input type="text" class="form-control" name="nationalcode" value="${employee.nationalcode}"/></td>
                            <td><input type="submit" class="btn btn-info" value="UPDATE"/></td>
                            <td><input type="button" class="btn btn-danger" value="DELETE"
                                       onclick="removeEmployee(${employee.id})"/></td>
                        </form>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>
<script>
    function removeEmployee(id) {
        if (confirm('r u sure?'))
            window.location = "/employee/delete.do?id=" + id;
    }
</script>
</body>
</html>
