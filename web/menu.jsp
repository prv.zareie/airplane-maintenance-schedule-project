<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">MyProject</a>
        </div>
        <ul class="nav navbar-nav">
            <li class="active">
                <a href="/index.jsp">Home</a>

            </li>
            <li><a href="/employee/findAll.do">Employee</a></li>
            <li><a href="/skill/findAll.do">Skill</a></li>
            <li><a href="/plane/findAll.do">Plane</a></li>
            <li><a href="/checkup/findAll.do">Checkup</a></li>
            <li><a href="/empSkill/findAll.do">Employee Skill</a></li>
            <li><a href="/checkSche/findAll.do">Checkup Schedule</a></li>
        </ul>
    </div>
</nav>
