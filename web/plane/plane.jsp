<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Plane Panel</title>
    <link rel="stylesheet" href="/assets/bootstrap.min.css">
    <script src="/assets/jquery.min.js"></script>
    <script src="/assets/bootstrap.min.js"></script>
</head>
<body>
<jsp:include page="/menu.jsp"/>
<div class="container">
    <div class="panel panel-primary">
        <div class="panel-heading">Plane Panel</div>
        <div class="panel-body">
            <form action="/plane/save.do">
                <input type="text" placeholder="code" class="form-control" name="code"/>
                <input type="submit" class="btn btn-info" value="SAVE"/>
            </form>

            <table style="width: 100%" class="table table-hover">
                <thead>
                <tr>
                    <td>ID</td>
                    <td>CODE</td>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${requestScope.list}" var="plane">
                    <tr>
                        <form action="/plane/update.do">
                            <td><input readonly class="form-control" type="text" name="id" value="${plane.id}"/></td>
                            <td><input type="text" class="form-control" name="code" value="${plane.code}"/></td>
                            <td><input type="submit" class="btn btn-info" value="UPDATE"/></td>
                            <td><input type="button" class="btn btn-danger" value="DELETE"
                                       onclick="removePlane(${plane.id})"/></td>
                        </form>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>
<script>
    function removePlane(id) {
        if (confirm('r u sure?'))
            window.location = "/plane/delete.do?id=" + id;
    }
</script>
</body>
</html>
