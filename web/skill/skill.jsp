<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Skill Panel</title>
    <link rel="stylesheet" href="/assets/bootstrap.min.css">
    <script src="/assets/jquery.min.js"></script>
    <script src="/assets/bootstrap.min.js"></script>
</head>
<body>
<jsp:include page="/menu.jsp"/>
<div class="container">
    <div class="panel panel-primary">
        <div class="panel-heading">Skill Panel</div>
        <div class="panel-body">
            <form action="/skill/save.do">
                <input type="text" placeholder="description" class="form-control" name="description"/>
                <input type="submit" class="btn btn-info" value="SAVE"/>
            </form>

            <table style="width: 100%" class="table table-hover">
                <thead>
                <tr>
                    <td>ID</td>
                    <td>DESCRITPION</td>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${requestScope.list}" var="skill">
                    <tr>
                        <form action="/skill/update.do">
                            <td><input readonly class="form-control" type="text" name="id" value="${skill.id}"/></td>
                            <td><input type="text" class="form-control" name="description" value="${skill.description}"/></td>
                            <td><input type="submit" class="btn btn-info" value="UPDATE"/></td>
                            <td><input type="button" class="btn btn-danger" value="DELETE"
                                       onclick="removeSkill(${skill.id})"/></td>
                        </form>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>
<script>
    function removeSkill(id) {
        if (confirm('r u sure?'))
            window.location = "/skill/delete.do?id=" + id;
    }
</script>
</body>
</html>
